set nocompatible
syntax on
set background=dark

set ignorecase
set smartcase

set backspace=indent,eol,start
set autoindent
set expandtab
set tabstop=2
"set softtabstop=2
set shiftwidth=2

set number

set cursorline

" Complete options (disable preview scratch window)
"set completeopt=menu,menuone,longest

" SuperTab option for context aware completion
let g:SuperTabDefaultCompletionType="context"
"let g:SuperTabCompletionContexts = ['s:ContextText', 's:ContextDiscover']
"let g:SuperTabContextTextOmniPrecedence = ['&omnifunc', '&completefunc']    
"let g:SuperTabContextDiscoverDiscovery = 
"    \ ["&completefunc:<c-x><c-u>", "&omnifunc:<c-x><c-o>"]

" Use libclang
let g:clang_use_library=1
" Show clang errors in the quickfix window
let g:clang_complete_copen=1 
let g:clang_complete_macros=1
" Disable auto popup, use <Tab> to autocomplete
"let g:clang_complete_auto=0
let g:clang_periodic_quickfix=1
"let g:clang_snippets=1
let g:clang_conceal_snippets=1
let g:clang_hl_errors=1
let g:clang_auto_select=1
nnoremap <Leader>q :call g:ClangUpdateQuickFix()<CR>

" Highlight on overlenght
if exists('+colorcolumn')
	set colorcolumn=79
	highlight link OverLength ColorColumn
	exec 'match OverLength /\%'.&cc.'v.\+/'
endif


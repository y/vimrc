%  goto matching bracket / comment
* or # find occurance of word under cursor
f<char>  find and goto <char>
t<char>  find and goto until before <char>
\q	trigger clang quick fix
cc[!] [N]	goto N-th entry in quick fix
cn	goto next entry in quick fix
cp	goto prev entry in quick fix
K   lookup man pages for identifier under cursor
L   goto last line on the screen
H   goto first line on the screen
gg  goto first line in the current file
G   goto last line in the current file
gg=G reindent the whole file

:%s/\<KEY_WORD\>/OTHER_WORD/gc    find/replace with confirmation

:vs         open file in new left split window
:sp         open file in new top split window
C-q         close current window
C-w C-w     switch to other windows
C-w r or R  rotate(?)
C-w h,j,k,l switch to window on left/down/up/right
C-w H,J,K,L move current window to left/down/up/right
C-w < or >  resize window horizontally
C-w + or -  resize window vertically
C-g         show info of current file
:set col=CC lines=LL
:set scb or noscb  scroll windows together
:set cc=80,120  set color column at 80 and 120
